# CI examples

This project contains CI examples for a fun workshop.
Examples use [GitLab](https://gitlab.com/)'s CI.

Each folder contains a different example.
To run an example, follow the instructions below.

## 1. First time setup

### Create your own copy of the project

To start experimenting,
make your own copy of `momosa/ci_examples` by forking the project.
Just click the `fork` button on the project homepage
and choose to fork the project under your user namespace.
See [GitLab's docs on Forks](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
for more detailed instructions.

### Verify your project's settings

#### General settings

Under `settings > general`,
make sure pages, container registry, and pipelines are enabled.

Your visibility and permission settings might look something like this:

![Visibility and permission settings](images/visibility_settings.png "Visibility and permission sessings")

#### Enable shared runners

Under `settings > CI/CD`,
enable shared runners.

![Runner settings](images/runner_settings.png "Runner settings")

## 2. Tell GitLab about the pipeline

Gitlab creates pipelines by looking for a file `.gitlab-ci.yml` in the project root.

Copy the `gitlab-ci.yml` into the project root directory as `.gitlab-ci.yml`.
Feel free to experiment with `.gitlab-ci.yml`.

```shell
cp examples/simple/gitlab-ci.yml .gitlab-ci.yml
```

## 3. Upload your changes

Add and push your changes.

```shell
git add .gitlab-ci.yml
git commit -m "Try out the simple example"
git push
```

## 4. See it in action

### Pipelines

See your pipeline in action on the Pipeline page through GitLab's UI.

If you create a Merge Request,
you'll see your most recent pipeline run there, too.

You can require that pipelines pass before merging.

### Docker images

The docker example will push an image to the project's docker registry.
You can find the registry under `Packages > Container Registry`
in the left panel.

To troubleshoot,
ensure your pipeline has run successfully
and that you've enable the container registry in general settings.

### Deployment

The deploy example is set to deploy to GitLab pages.
Your site should be visible at `https://<namespace>.gitlab.io/project_name/`.
For example, `https://momosa.gitlab.io/ci_examples/`.

To troubleshoot,
ensure your pipeline has run successfully
and you have pages enabled in general settings.

## 5. Experiment

It's time for _science_!
Experiment with pipelines to create one that automates your own tasks.
